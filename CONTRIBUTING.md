# Contributing

To build the project from source you need to [install flutter](https://flutter.io/get-started/install).

## Setup

```
$ git clone https://gitlab.com/neptronix/nepalmap
$ cd nepalmap
$ flutter packages get
```

Then you can either run the app in development/debug mode:
```
$ flutter run
```
or in release mode:
```
$ flutter run --release
```

## Editors and IDEs
* [Android Studio](https://flutter.io/get-started/editor/#androidstudio)
* [VSCode](https://flutter.io/get-started/editor/#vscode)

or any other text editor of your choice.

## Code Style
The code should satisfy all the [lint rules](http://dart-lang.github.io/linter/lints) specified in `analysis_options.yaml` file. A linter program is pre installed when the Dart plugin is installed for the above mentioned editors.
