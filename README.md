# NepalMap

A mobile app for [NepalMap](https://nepalmap.org/) created using [flutter](https://fluter.io/).

# Installation

Check out [releases](https://gitlab.com/neptronix/nepalmap/tags) to download pre built package file of your choice.