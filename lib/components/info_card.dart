import 'package:flutter/material.dart';
import 'package:NepalMap/containers/profile.dart';
import 'package:NepalMap/components/customroute.dart';
import 'package:NepalMap/utils.dart';

/// Shows the basic info on a location such as population,
/// its geographical parent(s) if present and census date.
class InfoCard extends StatelessWidget {
  /// Contains data of key "geography" from JSON API response.
  final Map<String, dynamic> data;

  /// The total population of the location.
  final int population;

  /// Creates a [InfoCard].
  const InfoCard(this.data, this.population);

  List<Widget> _generateInfo(BuildContext context) {
    String geoLevel = data['this']['geo_level'];
    geoLevel = geoLevel.replaceFirst(geoLevel[0], geoLevel[0].toUpperCase());

    final List<Widget> parents = <Widget>[];
    final Map<dynamic, dynamic> parentsMap = data['parents'];
    final int length = parentsMap.length;

    if (length > 0) {
      geoLevel = '$geoLevel in';
    }

    parentsMap.forEach((dynamic key, dynamic value) {
      parents.add(
        new GestureDetector(
          onTap: () {
            Navigator.of(context).push(
                  new CustomRoute<Profile>(
                    builder: (_) => new Profile(
                          code: value['full_geoid'],
                          name: value['name'],
                        ),
                  ),
                );
          },
          child: new Text(
            value['name'],
            style: new TextStyle(
              color: Theme.of(context).primaryColor,
              decoration: TextDecoration.underline,
            ),
          ),
        ),
      );

      if (length > 1) {
        parents.add(const Text(','));
        parents.add(new Container(width: 3.0));
      } else {
        parents.add(const Text('.'));
      }
    });

    return <Widget>[
      new Text(
        data['this']['name'],
        style: const TextStyle(
          fontSize: 32.0,
          fontWeight: FontWeight.bold,
        ),
      ),
      new Container(
        child: new Row(
          children: <Widget>[
            new Text(geoLevel),
            new Container(width: 3.0),
            new Row(
              children: parents,
            )
          ],
        ),
      ),
      new Container(height: 20.0),
      population == null
          ? const Text('')
          : new Text(
              formatNumber(population.toString()),
              style: const TextStyle(
                fontSize: 32.0,
                fontWeight: FontWeight.bold,
              ),
            ),
      const Text(
        'Population',
        style: const TextStyle(
          fontSize: 18.0,
        ),
      ),
      new Container(height: 20.0),
      new Container(
        child: new Row(
          children: <Widget>[
            const Text(
              'Census data:',
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            new Container(width: 3.0),
            const Text('2011'),
          ],
        ),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.all(10.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: _generateInfo(context),
      ),
    );
  }
}
