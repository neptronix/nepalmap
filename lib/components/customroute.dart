import 'package:flutter/material.dart';

/// It is a [MaterialPageRoute] with [SlideTransition].
class CustomRoute<T> extends MaterialPageRoute<T> {
  /// Creates a [CustomRoute] with the provided [WidgetBuilder].
  CustomRoute({@required WidgetBuilder builder}) : super(builder: builder);

  @override
  Widget buildTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return new SlideTransition(
      child: new Material(
        elevation: 16.0,
        child: child,
      ),
      position: new Tween<Offset>(
        begin: const Offset(1.0, 0.0),
        end: Offset.zero,
      ).animate(animation),
    );
  }
}
