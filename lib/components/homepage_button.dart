import 'package:flutter/material.dart';

/// Creates a [MaterialButton] with a leading 'OR' [Text].
class HomeButton extends StatelessWidget {
  /// The title of the [HomeButton].
  final String title;

  /// The top margin of the [HomeButton].
  final double top;

  /// Specifies weather the [HomeButton] is enabled or not.
  /// Default: true (enabled)
  final bool enabled;

  /// Specifies the left/right margin for the [HomeButton].
  final double margin;

  /// Callback function to be called after the [HomeButton] is pressed.
  final VoidCallback onPressed;

  /// Creates a [HomeButton].
  const HomeButton(
    this.title, {
    @required this.top,
    @required this.enabled,
    @required this.margin,
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: new EdgeInsets.only(top: top, left: margin, right: margin),
      child: Row(
        children: <Widget>[
          new Container(
            margin: new EdgeInsets.only(right: margin),
            child: const Text(
              'OR',
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          new Expanded(
            child: new MaterialButton(
              onPressed: enabled ? onPressed : null,
              color: Theme.of(context).primaryColor,
              child: new Container(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: new Text(
                  title,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
