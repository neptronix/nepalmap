import 'package:flutter/material.dart';

/// A header for the sub section.
class SubSectionHeader extends StatelessWidget {
  /// Title of the sub section.
  final String heading;

  /// Creates a header for the subsection.
  const SubSectionHeader(this.heading);

  @override
  Widget build(BuildContext context) {
    return new Align(
      alignment: Alignment.topLeft,
      child: new Container(
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.all(10.0),
        child: new Text(
          heading,
          style: const TextStyle(
            fontSize: 24.0,
          ),
        ),
      ),
    );
  }
}
