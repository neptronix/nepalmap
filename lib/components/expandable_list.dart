import 'package:flutter/material.dart';

/// A list of widgets that can be expanded and collapsed.
///
/// The list is initially collapsed.
class ExpandableList extends StatelessWidget {
  /// A title for the list.
  final String title;

  /// A list of children for the [ExpandableList].
  final List<Widget> children;

  /// Creates a [ExpandableList] with the provided title and list of children.
  const ExpandableList({@required this.title, @required this.children});

  @override
  Widget build(BuildContext context) {
    return new ExpansionTile(
      key: new PageStorageKey<String>(title),
      title: new Text(
        title,
        style: const TextStyle(fontSize: 20.0),
      ),
      initiallyExpanded: false,
      children: children,
    );
  }
}
