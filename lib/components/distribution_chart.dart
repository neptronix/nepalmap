import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:NepalMap/utils.dart';

/// List of [charts.Color] to be used in charts.
List<charts.Color> colors = <charts.Color>[
  const charts.Color(r: 224, g: 0, b: 21),
  const charts.Color(r: 0, g: 173, b: 239),
  const charts.Color(r: 98, g: 149, b: 204),
  const charts.Color(r: 221, g: 133, b: 192),
  const charts.Color(r: 142, g: 204, b: 35),
  const charts.Color(r: 252, g: 205, b: 6),
  const charts.Color(r: 219, g: 186, b: 151),
  const charts.Color(r: 170, g: 170, b: 170),
];

/// List of [Color] to be used in legends.
List<Color> normalColors = <Color>[
  const Color.fromRGBO(224, 0, 21, 1.0),
  const Color.fromRGBO(0, 173, 239, 1.0),
  const Color.fromRGBO(98, 149, 204, 1.0),
  const Color.fromRGBO(221, 133, 192, 1.0),
  const Color.fromRGBO(142, 204, 35, 1.0),
  const Color.fromRGBO(252, 205, 6, 1.0),
  const Color.fromRGBO(219, 186, 151, 1.0),
  const Color.fromRGBO(170, 170, 170, 1.0),
];

/// Represents a single data in a chart.
class Distribution {
  /// The label associated with the value.
  final String name;

  /// The numeric value of the data.
  final double value;

  /// Creates a [Distribution] class.
  Distribution(this.name, this.value);
}

/// Returns a [charts.PieChart] or [charts.BarChart] according to the
/// data values provided in the constructor.
class DistributionChart extends StatelessWidget {
  /// The data required to generate [Distribution] classes.
  final Map<dynamic, dynamic> data;

  /// Optional title for the chart.
  ///
  /// Default value is a empty string.
  final String title;

  /// Specifies is the chart is a pie chart or bar chart.
  /// if value of pie is true the [DistributionChart] returns a
  /// pie chart or a bar chart otherwise.
  ///
  /// Default value is false.
  final bool pie;

  /// Specifies weather the bar chart is a grouped chart or not.
  ///
  /// Default value is false.
  final bool grouped;

  /// Specifies the maximum number of columns in a bar chart.
  ///
  ///  Default value is 2.
  final int columns;

  /// Specifies the type nummerical values in [data] for proper formatting.
  /// Values can be 'percentage' or 'number'.
  /// Providing a type percentage adds a '%' symbol at the end of the value.
  /// Providing a type number formats the number with commas.
  ///
  /// Default value is percentage.
  final String type;

  /// Creates a [DistributionChart] with provided required/optional arguments.
  const DistributionChart(
    this.data, {
    this.title = '',
    this.pie = false,
    this.grouped = false,
    this.columns = 2,
    this.type = 'percentage',
  });

  List<charts.Series<Distribution, String>> _generateSeriesList() {
    List<Distribution> listData = <Distribution>[];
    final List<charts.Series<Distribution, String>> groupData =
        <charts.Series<Distribution, String>>[];

    if (grouped) {
      data.forEach((dynamic key, dynamic value) {
        if (key != 'metadata') {
          listData = <Distribution>[];
          value.forEach((dynamic key, dynamic value) {
            if (key != 'metadata') {
              final String groupName = value['name'];
              final double distributionValue = value['values']['this'];

              listData.add(new Distribution(groupName, distributionValue));
            }
          });

          groupData.add(
            new charts.Series<Distribution, String>(
              id: key,
              domainFn: (Distribution group, _) => group.name,
              measureFn: (Distribution group, _) => group.value,
              data: listData,
            ),
          );
        }
      });

      return groupData;
    } else {
      data.forEach((dynamic key, dynamic value) {
        if (key != 'metadata') {
          final String groupName = value['name'];
          final double distributionValue = value['values']['this'];

          listData.add(new Distribution(groupName, distributionValue));
        }
      });

      return <charts.Series<Distribution, String>>[
        new charts.Series<Distribution, String>(
          id: 'Distribution',
          colorFn: (_, int number) => pie
              ? colors[number % colors.length]
              : const charts.Color(r: 224, g: 0, b: 21),
          domainFn: (Distribution group, _) => group.name,
          measureFn: (Distribution group, _) => group.value,
          labelAccessorFn: (Distribution group, _) => type == 'percentage'
              ? '${group.value}%'
              : '${formatNumber(group.value.round().toString())}',
          data: listData,
        )
      ];
    }
  }

  List<List<String>> _generatePieLegend() {
    final List<List<String>> legendData = <List<String>>[];

    data.forEach((dynamic key, dynamic value) {
      if (key != 'metadata') {
        final String groupName = value['name'];
        String distributionValue = value['values']['this'].toString();
        if (type == 'percentage') distributionValue = '$distributionValue%';

        legendData.add(<String>[groupName, distributionValue]);
      }
    });

    return legendData;
  }

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        title == ''
            ? new Container()
            : new Align(
                alignment: Alignment.center,
                child: new Container(
                  margin: const EdgeInsets.only(
                    top: 10.0,
                    left: 10.0,
                    right: 10.0,
                  ),
                  child: new Text(
                    title,
                    style: const TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
        new Container(
          child: pie == true
              ? new Container(
                  child: Column(
                    children: <Widget>[
                      new Container(
                        height: MediaQuery.of(context).size.height / 3,
                        child: new charts.PieChart<String>(
                          _generateSeriesList(),
                          defaultRenderer: new charts.ArcRendererConfig<String>(
                              arcWidth: 40),
                          defaultInteractions: true,
                          animate: false,
                        ),
                      ),
                      new Container(child: new PieLegend(_generatePieLegend())),
                    ],
                  ),
                )
              : new Container(
                  child: grouped
                      ? new charts.BarChart(
                          _generateSeriesList(),
                          defaultRenderer: new charts.BarRendererConfig(
                            groupingType: charts.BarGroupingType.grouped,
                          ),
                          behaviors: <charts.SeriesLegend>[
                            new charts.SeriesLegend(
                              position: charts.BehaviorPosition.bottom,
                              desiredMaxColumns: columns,
                            )
                          ],
                          vertical: true,
                          animate: false,
                        )
                      : new charts.BarChart(
                          _generateSeriesList(),
                          barRendererDecorator:
                              new charts.BarLabelDecorator<String>(),
                          vertical: false,
                          animate: false,
                        ),
                  height: MediaQuery.of(context).size.height / 2,
                ),
          margin: const EdgeInsets.all(10.0),
        ),
      ],
    );
  }
}

/// A list of [LegendRect] for a [charts.PieChart.]
class PieLegend extends StatelessWidget {
  /// The list data needed to create a [PieLegend].
  /// An item in the list includes a name
  /// followed by a string representation of the value.
  final List<List<String>> data;

  /// Creates a [PieLegend] using the provided [data].
  const PieLegend(this.data);

  List<Widget> _generateLegends() {
    final List<LegendRect> legends = <LegendRect>[];

    int index = 0;
    for (List<String> item in data) {
      legends.add(new LegendRect(item[0], item[1], normalColors[index]));
      index++;
    }

    if (legends.isEmpty)
      return <Widget>[new Container()];
    else
      return legends;
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: const EdgeInsets.only(bottom: 30.0),
      child: new Column(
        children: _generateLegends(),
      ),
    );
  }
}

/// A small colored square with a caption to use as a
/// legend for [charts.PieChart].
class LegendRect extends StatelessWidget {
  /// The title of the legend.
  final String title;

  /// The value associated with the legend.
  final String value;

  /// The color of the rectangle representing a legend.
  final Color color;

  /// Creates a [LegendRect] with the provided title, value and color.
  const LegendRect(this.title, this.value, this.color);

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          new Container(
            child: new Row(
              children: <Widget>[
                new Container(
                  height: 12.0,
                  width: 12.0,
                  color: color,
                ),
                new Container(width: 10.0),
                new Text(title),
              ],
            ),
          ),
          new Container(width: 10.0),
          new Expanded(
            child: new Container(
              height: 1.0,
              color: Colors.black12,
            ),
          ),
          new Container(width: 10.0),
          new Text(value),
        ],
      ),
    );
  }
}
