import 'package:flutter/material.dart';
import 'package:NepalMap/utils.dart';

/// Widget to show statistics.
class StatList extends StatelessWidget {
  /// Statistical data.
  final Map<dynamic, dynamic> data;

  /// Type of the data.
  final String type;

  /// The name of the data given if the JSON API doesn't have the infornamtion
  final String name;

  /// Title of the data.
  final String title;

  /// Default constructor for the [StatList].
  const StatList(this.data, this.type, {this.name = '', this.title = ''});

  Widget _getPhrase(dynamic a, dynamic b) {
    List<String> phrase;
    try {
      phrase = nearest(num.parse(a) / num.parse(b));
    } catch (e) {
      try {
        phrase = nearest(a / b);
      } catch (e) {
        return new Container();
      }
    }

    const TextStyle style = const TextStyle(
      fontSize: 16.0,
    );

    return new Row(
      children: <Widget>[
        new Text(phrase[0], style: style.copyWith(fontWeight: FontWeight.bold)),
        new Container(width: 3.0),
        new Text(phrase[1], style: style),
        new Container(width: 3.0),
        new Text(
            'the rate in Nepal: ${_formattedValue(b.toString(), ignoreName: false)}',
            style: style)
      ],
    );
  }

  String _formattedValue(String value, {bool ignoreName = true}) {
    switch (type) {
      case 'percentage':
        return '$value%';
      case 'dollar':
        return '\$${formatNumber(value)}';
      case 'name':
        if (ignoreName) {
          return '';
        }

        return value;
      case 'number':
        return formatNumber(value);
      default:
        return value;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: const EdgeInsets.symmetric(horizontal: 10.0),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          title == ''
              ? new Container()
              : new Text(
                  title,
                  style: const TextStyle(
                    fontSize: 22.0,
                  ),
                ),
          new Container(height: 10.0),
          new Text(
            _formattedValue(data['values']['this'].toString()),
            style: new TextStyle(
              fontSize: type == 'name' ? 0.0 : 32.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          new Text(
            data['name'].toString(),
            style: new TextStyle(
              fontSize: type == 'name' ? 32.0 : 20.0,
              fontWeight: type == 'name' ? FontWeight.bold : FontWeight.normal,
            ),
          ),
          name != ''
              ? new Container(
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        name,
                        style: const TextStyle(fontSize: 20.0),
                      ),
                      new Container(height: 10.0),
                    ],
                  ),
                )
              : new Container(height: 10.0),
          _getPhrase(data['values']['this'], data['values']['country']),
          new Container(height: 10.0),
        ],
      ),
    );
  }
}
