import 'dart:async';
import 'dart:convert';

import 'package:NepalMap/components/info_card.dart';
import 'package:NepalMap/components/search_ui.dart';
import 'package:NepalMap/sections/agriculture.dart';
import 'package:NepalMap/sections/business.dart';
import 'package:NepalMap/sections/demographics.dart';
import 'package:NepalMap/sections/development.dart';
import 'package:NepalMap/sections/disasters.dart';
import 'package:NepalMap/sections/education.dart';
import 'package:NepalMap/sections/elections.dart';
import 'package:NepalMap/sections/forests.dart';
import 'package:NepalMap/sections/health.dart';
import 'package:NepalMap/sections/households.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

/// Profile page for the locations.
class Profile extends StatelessWidget {
  /// Full geo id of the location.
  final String code;

  /// Full name of the location.
  final String name;

  /// Default constructor for [Profile].
  const Profile({@required this.code, @required this.name});

  Future<List<Widget>> _getData(BuildContext context, String code) async {
    final String url = 'https://nepalmap.org/profiles/'
        '$code-${name.toLowerCase().replaceAll(' ', '-')}.json';
    final http.Response response = await http.get(url);

    final dynamic decodedResponse = json.decode(response.body);

    int population;

    if (decodedResponse['demographics']['has_data'] == true) {
      population =
          decodedResponse['demographics']['total_population']['values']['this'];
    } else if (decodedResponse['demographics']['area_has_data'] == true) {
      population =
          decodedResponse['demographics']['total_population']['values']['this'];
    }

    final List<Widget> widgetsList = <Widget>[
      new InfoCard(decodedResponse['geography'], population),
      getDemographics(decodedResponse['demographics']),
      getElections(decodedResponse['elections']),
      getEducation(decodedResponse['education']),
      getHealth(decodedResponse['health']),
      getHouseholds(decodedResponse['households']),
      getBusiness(decodedResponse['business']),
      getAgriculture(decodedResponse['agriculture']),
      getForests(decodedResponse['forests']),
      getDisasters(decodedResponse['disasters']),
      getDevelopment(decodedResponse['development']),
    ];

    return widgetsList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(name),
        actions: <Widget>[
          new IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return new Container(
                    child: new SearchUI(),
                  );
                },
              );
            },
          ),
        ],
      ),
      body: new FutureBuilder<List<Widget>>(
        future: _getData(context, code),
        builder: (BuildContext context, AsyncSnapshot<List<Widget>> snapshot) {
          if (!snapshot.hasData) {
            return const Center(
              child: const CircularProgressIndicator(),
            );
          }

          return new ListView.builder(
            itemBuilder: (BuildContext context, int index) {
              return snapshot.data[index];
            },
            itemCount: snapshot.data.length,
          );
        },
      ),
    );
  }
}
