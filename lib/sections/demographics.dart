import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/subsections/demographics/age.dart';
import 'package:NepalMap/subsections/demographics/caste_and_ethnic.dart';
import 'package:NepalMap/subsections/demographics/citizenship.dart';
import 'package:NepalMap/subsections/demographics/disabled.dart';
import 'package:NepalMap/subsections/demographics/language.dart';
import 'package:NepalMap/subsections/demographics/life_expectancy_income.dart';
import 'package:NepalMap/subsections/demographics/population.dart';
import 'package:flutter/material.dart';

/// Returns a [ExpandableList] with demographics data.
Widget getDemographics(Map<dynamic, dynamic> data) {
  final List<Widget> demographics = <Widget>[];

  if (data['has_data'] != null && data['has_data'] == true) {
    demographics.add(new Population(data));

    if (data['is_vdc'] != null && data['is_vdc'] == false) {
      demographics.addAll(<Widget>[
        new Age(data),
        new LifeExpectancyAndIncome(data),
        new Language(data),
        new Citizenship(data),
        new CasteAndEthnicGroup(data),
      ]);
    }
    demographics.add(new Disabled(data));

    return new ExpandableList(
      title: 'Demographics',
      children: demographics,
    );
  }

  return new Container(
    margin: const EdgeInsets.only(top: 20.0),
    child: const Center(
      child: const Text('No data availble for this area.'),
    ),
  );
}
