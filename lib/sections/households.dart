import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/subsections/households/construction_materials.dart';
import 'package:NepalMap/subsections/households/cooking_fuel.dart';
import 'package:NepalMap/subsections/households/drinking_water.dart';
import 'package:NepalMap/subsections/households/home_ownership.dart';
import 'package:NepalMap/subsections/households/households_data.dart';
import 'package:NepalMap/subsections/households/lighting_fuel.dart';
import 'package:NepalMap/subsections/households/toilet_type.dart';
import 'package:flutter/material.dart';

/// Returns a [ExpandableList] with elections data.
Widget getHouseholds(Map<dynamic, dynamic> data) {
  final List<Widget> households = <Widget>[];

  if (data['area_has_data'] != null && data['area_has_data'] == true) {
    households.addAll(<Widget>[
      new HouseholdsData(data),
      new ConstructionMaterials(data),
      new HomeOwnership(data),
      new CookingFuel(data),
      new LightingFuel(data),
      new DrinkingWater(data),
      new ToiletType(data),
      new DistributionChart(
        data['household_facilities'],
        title: 'Facilities available by household',
      ),
    ]);

    return new ExpandableList(
      title: 'Households',
      children: households,
    );
  }

  return new Container();
}
