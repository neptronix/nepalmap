import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:flutter/material.dart';

/// Returns a [ExpandableList] with elections data.
Widget getElections(Map<dynamic, dynamic> data) {
  final List<Widget> elections = <Widget>[];

  if (data['area_has_data'] != null && data['area_has_data'] == true) {
    elections.addAll(<Widget>[
      new StatList(
        data['total_voters'],
        'number',
        title: 'Voters',
      ),
      new DistributionChart(
        data['voter_distribution'],
        pie: true,
        title: 'Voters by sex',
      ),
      const Divider(),
      new StatList(
        data['total_electoral_bodies'],
        'number',
        title: 'Local Electoral Bodies',
      ),
      new DistributionChart(
        data['local_electoral_bodies_distribution'],
        pie: true,
        title: 'Local electoral bodies',
      ),
      const Divider(),
      new StatList(
        data['polling_places'],
        'number',
        title: 'Polling Places and Registered Political Parties',
      ),
      new StatList(
        data['political_parties'],
        'number',
      ),
      const Divider(),
      new StatList(
        data['total_mayoralities'],
        'float',
        title: 'Mayoral Seats by Party',
      ),
      new DistributionChart(
        data['mayoral_party_distribution'],
        title: 'Mayoral seats held by party',
      ),
      const Divider(),
      new StatList(
        data['total_deputy_mayoralities'],
        'float',
        title: 'Deputy Mayoral Seats by Party',
      ),
      new DistributionChart(
        data['deputy_mayoral_party_distribution'],
        title: 'Deputy mayoral seats held by party',
      ),
    ]);

    return new ExpandableList(
      title: 'Elections',
      children: elections,
    );
  }

  return new Container();
}
