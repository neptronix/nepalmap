import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/subsections/agriculture/livestock.dart';
import 'package:NepalMap/subsections/agriculture/meat_production.dart';
import 'package:flutter/material.dart';

/// Returns a [ExpandableList] with agricultural data.
Widget getAgriculture(Map<dynamic, dynamic> data) {
  final List<Widget> agriculture = <Widget>[];

  if (data['area_has_data'] != null && data['area_has_data'] == true) {
    agriculture.addAll(<Widget>[
      new Livestock(data),
      new MeatProduction(data),
    ]);

    return new ExpandableList(
      title: 'Agriculture',
      children: agriculture,
    );
  }

  return new Container();
}
