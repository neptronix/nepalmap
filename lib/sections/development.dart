import 'package:NepalMap/components/expandable_list.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:flutter/material.dart';

/// Returns a [ExpandableList] with development data.
Widget getDevelopment(Map<dynamic, dynamic> data) {
  final List<Widget> development = <Widget>[];

  if (data['area_has_data'] != null && data['area_has_data'] == true) {
    development.addAll(<Widget>[
      new StatList(
        data['district_aid_amounts'],
        'dollar',
        title: 'District-level Development Assistance',
      ),
      new StatList(
        data['district_aid_projects'],
        'number',
      ),
    ]);

    return new ExpandableList(
      title: 'Development',
      children: development,
    );
  }

  return Container();
}
