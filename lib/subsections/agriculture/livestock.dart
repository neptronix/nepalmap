import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on livestocks.
class Livestock extends StatelessWidget {
  /// Contains data of key "agriculture" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [Livestock].
  const Livestock(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Livestock'),
        new StatList(
          data['total_livestock'],
          'number',
        ),
        new DistributionChart(
          data['livestock_distribution'],
          title: 'Livestock',
          type: 'number',
        ),
        const Divider(),
      ],
    );
  }
}
