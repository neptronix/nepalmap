import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on meat production.
class MeatProduction extends StatelessWidget {
  /// Contains data of key "agriculture" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [MeatProduction].
  const MeatProduction(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Meat Production'),
        new StatList(
          data['total_meat'],
          'number',
        ),
        new DistributionChart(
          data['meat_distribution'],
          title: 'Metric tons of meat produced',
          type: 'number',
        ),
      ],
    );
  }
}
