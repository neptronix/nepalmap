import 'package:flutter/material.dart';
import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on different castes and
/// ethnic groups in the location.
class CasteAndEthnicGroup extends StatelessWidget {
  /// Contains data of key "demographics" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [CasteAndEthnicGroup].
  const CasteAndEthnicGroup(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SubSectionHeader('Caste and Ethnic Groups'),
          new StatList(
            data['most_populous_caste'],
            'name',
            name: 'Most populous caste or ethnic group',
          ),
          new DistributionChart(
            data['ethnic_distribution'],
            title: 'Population by caste or ethnic group',
          ),
          const Divider(),
        ],
      ),
    );
  }
}
