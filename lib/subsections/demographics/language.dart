import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on different languages in the location.
class Language extends StatelessWidget {
  /// Contains data of key "demographics" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Create a vertical [Column] list for details on the languages.
  const Language(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          const SubSectionHeader('Language'),
          new StatList(data['language_most_spoken'], 'name',
              name: 'Language most spoken at home'),
          new DistributionChart(data['language_distribution'],
              title: 'Population by language most spoken at home'),
          const Divider(),
        ],
      ),
    );
  }
}
