import 'package:flutter/material.dart';

import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on life expectancy
/// and income of the population.
class LifeExpectancyAndIncome extends StatelessWidget {
  /// Contains data of key "demographics" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [LifeExpectancyAndIncome] widget.
  const LifeExpectancyAndIncome(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          const SubSectionHeader('Life Expectancy and Income'),
          new StatList(data['life_expectancy'], 'float'),
          new StatList(data['per_capita_income'], 'dollar'),
          const Divider(),
        ],
      ),
    );
  }
}
