import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on population of the location.
class Population extends StatelessWidget {
  /// Contains data of key "demographics" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [Population] widget.
  const Population(this.data);

  Widget _getDistrictSpecific() {
    return new Container(
      child: new Column(
        children: <Widget>[
          new StatList(data['pop_projection_2031'], 'number'),
          new DistributionChart(data['pop_2031_dist'],
              title: 'Projected by sex in 2031', pie: true),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          const SubSectionHeader('Population'),
          new StatList(data['total_population'], 'number'),
          new DistributionChart(data['sex_ratio'], title: 'Sex', pie: true),
          data['is_vdc'] ? new Container() : _getDistrictSpecific(),
          const Divider(),
        ],
      ),
    );
  }
}
