import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the deta on distribution of citizenship.
class Citizenship extends StatelessWidget {
  /// Contains data of key "demographics" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [Citizenship].
  const Citizenship(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          const SubSectionHeader('Citizenship'),
          new DistributionChart(data['citizenship_distribution'], pie: true),
          new DistributionChart(
            data['citizenship_by_sex'],
            title: 'Population by citizenship and sex',
            grouped: true,
          ),
          const Divider(),
        ],
      ),
    );
  }
}
