import 'package:flutter/material.dart';

import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on disabled population.
class Disabled extends StatelessWidget {
  /// Contains data of key "demographics" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [Disabled] widget.
  const Disabled(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          const SubSectionHeader('Disabled'),
          new StatList(
            data['percent_disabled'],
            'percentage',
          ),
          new DistributionChart(
            data['disability_ratio'],
            title: 'Disability',
            pie: true,
          ),
        ],
      ),
    );
  }
}
