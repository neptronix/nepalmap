import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on source of drinking water.
class DrinkingWater extends StatelessWidget {
  /// Contains value of key "households" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [DrinkingWater].
  const DrinkingWater(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Drinking Water Source '),
        new StatList(
          data['piped_tap'],
          'percentage',
        ),
        new DistributionChart(
          data['drinking_water_distribution'],
          title: 'Households by drinking water source',
        ),
        const Divider(),
      ],
    );
  }
}
