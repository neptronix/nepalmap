import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on the lighting fuel usage.
class LightingFuel extends StatelessWidget {
  /// Contains value of key "households" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Creates [Column] list to show details on lighting fuel.
  const LightingFuel(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Main Type of Lighting Fuel'),
        new StatList(
          data['lighting_electricity'],
          'percentage',
        ),
        new DistributionChart(
          data['lighting_fuel_distribution'],
          title: 'Households by main type of lighting fuel',
        ),
        const Divider(),
      ],
    );
  }
}
