import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on the fuel used for cooking.
class CookingFuel extends StatelessWidget {
  /// Contains value of key "households" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [CookingFuel].
  const CookingFuel(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Main Type of Cooking Fuel'),
        new StatList(
          data['cooking_wood'],
          'percentage',
        ),
        new DistributionChart(
          data['cooking_fuel_distribution'],
          title: 'Households by main type of cooking fuel',
        ),
        const Divider(),
      ],
    );
  }
}
