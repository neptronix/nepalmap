import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on the constuction materials
/// used to build houses.
class ConstructionMaterials extends StatelessWidget {
  /// Contains value of key "households" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Creates [Column] list to show details for construction materials.
  const ConstructionMaterials(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Household Construction Material'),
        new DistributionChart(
          data['foundation_type_distribution'],
          pie: true,
          title: 'Households by type of foundation',
        ),
        new DistributionChart(
          data['outer_wall_type_distribution'],
          pie: true,
          title: 'Households by type of wall',
        ),
        new DistributionChart(
          data['roof_type_distribution'],
          pie: true,
          title: 'Households by type of roof',
        ),
        const Divider(),
      ],
    );
  }
}
