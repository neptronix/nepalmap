import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on the types of toilet.
class ToiletType extends StatelessWidget {
  /// Contains value of key "households" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Creates [Column] list to show details for on the types of toilet.
  const ToiletType(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Household Toilet Type'),
        new StatList(
          data['flush_toilet'],
          'percentage',
        ),
        new DistributionChart(
          data['toilet_type_distribution'],
          title: 'Households by toilet type',
        ),
        const Divider(),
      ],
    );
  }
}
