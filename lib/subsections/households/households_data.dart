import 'package:flutter/material.dart';

import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on the numer and size of households.
class HouseholdsData extends StatelessWidget {
  /// Contains value of key "households" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [HouseholdsData].
  const HouseholdsData(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Households'),
        new StatList(data['total_households'], 'number'),
        new StatList(data['average_household_size'], 'float'),
        new StatList(data['male_to_female_ratio'], 'float'),
        const Divider(),
      ],
    );
  }
}
