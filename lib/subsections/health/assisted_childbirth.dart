import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on "Assisted Child Birth".
class AssistedChildbirth extends StatelessWidget {
  /// Contains data of key "health" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [AssistedChildbirth].
  const AssistedChildbirth(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Assisted Child Birth'),
        new StatList(data['total_assisted_childbirths'], 'number'),
        new DistributionChart(
          data['childbirth_assistance_distribution'],
          pie: true,
          title: 'Assisted Childbirth',
        ),
        new DistributionChart(
          data['maternal_death_in_childbirth_distribution'],
          title: 'Reported Maternal Deaths in Childbirth',
        ),
        new DistributionChart(
          data['health_facilities_data'],
          title: 'Health Facilities',
          type: 'number',
        ),
      ],
    );
  }
}
