import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on registrations by
/// different significant events in history.
class RegisterationEra extends StatelessWidget {
  /// Contains data of key "business" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [RegisterationEra].
  const RegisterationEra(this.data);

  @override
  Widget build(BuildContext context) {
    return new Container(
      child: new Column(
        children: <Widget>[
          const SubSectionHeader('Business registrations by era'),
          new StatList(
            data['total_companies_registered'],
            'number',
          ),
          new DistributionChart(
            data['registered_company_era_distribution'],
            title: 'All company registrations',
            type: 'number',
          ),
          new DistributionChart(
            data['registered_private_company_era_distribution'],
            title: 'Private company registrations',
            type: 'number',
          ),
          new DistributionChart(
            data['registered_public_company_era_distribution'],
            title: 'Public company registrations',
            type: 'number',
          ),
          new DistributionChart(
            data['registered_foreign_company_era_distribution'],
            title: 'Foreign company registrations',
            type: 'number',
          ),
          new DistributionChart(
            data['registered_non_profit_era_distribution'],
            title: 'Non-profit registrations',
            type: 'number',
          ),
        ],
      ),
    );
  }
}
