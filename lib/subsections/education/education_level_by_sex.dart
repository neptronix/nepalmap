import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on education level by sex.
class EducationLevelBySex extends StatelessWidget {
  /// Contains value of key "education" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [EducationLevelBySex] widget.
  const EducationLevelBySex(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Education Level Passed by Sex'),
        new StatList(data['primary_level_reached_by_sex'], 'percentage'),
        new DistributionChart(
          data['education_level_by_sex_distribution'],
          grouped: true,
          title: 'Highest education level reached',
        ),
        const Divider(),
      ],
    );
  }
}
