import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/stat_list.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on field of study of the population.
class FieldOfStudy extends StatelessWidget {
  /// Contains value of key "education" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [FieldOfStudy] widget.
  const FieldOfStudy(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Field of Study Above SLC'),
        new StatList(data['pop_above_slc'], 'number'),
        new DistributionChart(
          data['field_of_study_by_sex_distribution'],
          grouped: true,
          columns: 3,
          title: 'Top fields of study by sex',
        ),
        new DistributionChart(
          data['field_of_study_distribution'],
          title: 'Field of study',
        ),
        const Divider(),
      ],
    );
  }
}
