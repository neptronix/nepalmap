import 'package:flutter/material.dart';

import 'package:NepalMap/components/distribution_chart.dart';
import 'package:NepalMap/components/sub_section_header.dart';

/// Widget to show the details on literacy rate of the population.
class Literacy extends StatelessWidget {
  /// Contains value of key "education" from JSON API response.
  final Map<dynamic, dynamic> data;

  /// Default constructor for [Literacy] widget.
  const Literacy(this.data);

  @override
  Widget build(BuildContext context) {
    return new Column(
      children: <Widget>[
        const SubSectionHeader('Literacy'),
        new DistributionChart(data['literacy_distribution'], pie: true),
        new DistributionChart(
          data['literacy_by_sex_distribution'],
          grouped: true,
          title: 'Literacy rate by sex',
        ),
        const Divider(),
      ],
    );
  }
}
