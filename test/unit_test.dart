import 'package:NepalMap/utils.dart';
import 'package:test/test.dart';

void main() {
  test('nearest(0.05) returns less than 10 percent of', () async {
    expect(nearest(0.05), equals(<String>['less than 10 percent', 'of']));
  });

  test('nearest(1.15) returns about 20 percent higher than', () async {
    expect(nearest(1.15), equals(<String>['about 20 percent higher', 'than']));
  });

  test('nearest(1.62) returns more than 1.5 times', () async {
    expect(nearest(1.62), equals(<String>['more than 1.5 times', '']));
  });

  test('formatNumber(1234567890) returns 1,234,567,890', () async {
    expect(formatNumber('1234567890'), equals('1,234,567,890'));
  });

  test('formatNumber(1234) returns 1,234', () async {
    expect(formatNumber('1234'), equals('1,234'));
  });

  test('formatNumber(123) returns 123', () async {
    expect(formatNumber('123'), equals('123'));
  });
}
